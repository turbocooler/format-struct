//! Provides types and traits for representing endianness of an integer encoding.

mod sealed {
    pub trait Sealed {}
}

/// Represents endianness of an integer encoding.
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Endian {
    /// Little-endian representation.
    Little,
    /// Big-endian representation.
    Big,
}

/// Trait for types that represent fixed endianness.
pub trait FixedEndian: sealed::Sealed + Copy {
    /// Fixed endianness value of the type.
    const ENDIAN: Endian;
}

/// Represents big endian integer encoding.
#[derive(Default, Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct BigEndian;

impl sealed::Sealed for BigEndian {}

impl FixedEndian for BigEndian {
    const ENDIAN: Endian = Endian::Big;
}

/// Represents little endian integer encoding.
#[derive(Default, Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct LittleEndian;

impl sealed::Sealed for LittleEndian {}

impl FixedEndian for LittleEndian {
    const ENDIAN: Endian = Endian::Little;
}
