# format-struct

[![crate](https://img.shields.io/crates/v/format-struct.svg)](https://crates.io/crates/format-struct)
[![documentation](https://docs.rs/format-struct/badge.svg)](https://docs.rs/format-struct)

A library for quick and easy format structure definitions for use in binary file parsing.

See crate [docs](https://docs.rs/format-struct) for more info and usage examples.

## Features

* `std`: Implements `std::error::Error` on all errors (one error) provided by this crate.

## Minimum Supported Rust Version

This crate requires Rust 1.65 at a minimum.

The MSRV may be changed in the future, but it will be accompanied by a minor version bump.

## License

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Contributions are welcome and should be submitted as merge requests to this repository.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.