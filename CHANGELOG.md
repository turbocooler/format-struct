# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.5.0
### Added
- `format_struct` now supports nested types which implement `ReprByteSlice`.
- `ReprByteSlice` is now implemented for arrays of types implementing `ReprByteSlice`.

## Changed
- Bumped MSRV to 1.65.

## 0.4.0
### Added
- `(from|as)_byte_array(_mut)?` methods added to generated structures to allow conversion from/to byte arrays of appropriate sizes.

### Fixed
- `format_struct!` macro now accepts named constants and constant expressions as array sizes.

### Changed
- Use edition 2021 instead of 2018 (MSRV was 1.56 already so not bumped).

### Removed
- Manual implementation of `Debug` on generated structures.

### Fixed
- Fixes a bug where the `format-struct!` macro not being imported into the namespace would cause the macro to fail.

## 0.3.1
### Changed
- Changed `Debug` implementation on integer wrappers with fixed endian to print their actual value instead of the bytes.

## 0.3.0
This release has replaced the old `FromByteSlice` trait with a new `ReprByteSlice` trait that provides the mean to 
convert between the conforming types and byte slices in generic code. That allows to use the generated structures
with APIs like `BorrowedBuf` more efficiently.

### Added
- `into_bytes` methods for integer wrappers and generated structures.
- `safe_size_to_count` and `safe_count_to_size` utility functions.
- A test for `uninit_*` functions.

### Changed
- `FromByteSlice` trait was renamed to `ReprByteSlice`.
- Most of the conversion methods have moved into the `ReprByteSlice` trait.

## 0.2.3
### Added
- `uninit_slice_as_byte_slice` and `uninit_slice_as_byte_slice_mut` methods on integer wrappers and generated structures to simplify usage with `BorrowedBuf`.

## 0.2.2
### Changed
- Most construction methods on integer wrappers and some conversion methods on format structs are now const.

## 0.2.1
### Fixed
- Comments on fields can now span multiple lines.

## 0.2.0
This release changed the representation of structure integer fields from byte arrays to wrappers. This removes member
namespace pollution when a structure is used in the same module where it is defined and also allows for flexible 
access patterns where e.g. one field should always be big endian. The other nice feature is the possibility to have
dynamic endianness for fields: a structure may be parsed differently depending on a magic value.

### Added
- `from_bytes` method to generated structures and integer wrapper types.
- `slice_as_byte_slice(_mut)?` methods for integer wrappers.

### Changed
- Structures now represent integer fields using wrapper types rather than byte arrays.

### Removed
- Dependency on `paste`.

## 0.1.1
### Added
- `slice_as_byte_slice` and `slice_as_byte_slice_mut` methods on generated structures for simpler conversion.

## 0.1.0
Initial release.