// Main thing this checks for is if the macro works without anything being imported into the
// namespace.
format_struct::format_struct! {
    #[derive(Copy, Clone, Eq, PartialEq, Debug)]
    /// Outer doc.
    pub struct dynamic Foo {
        /// Inner doc.
        pub bar: u32,
    }
}

// This mostly ensures the thing compiles properly.
#[test]
fn integration() {
    assert_eq!(
        Foo::from_bytes([11, 0, 0, 0])
            .bar
            .get_with_endian(format_struct::Endian::Little),
        11
    );
    assert_eq!(
        Foo::from_bytes([11, 0, 0, 0])
            .bar
            .get_with_endian(format_struct::Endian::Big),
        11 << 24
    );
}
